/**************************************************************************************************
 * THE OMEGA LIB PROJECT
 *-------------------------------------------------------------------------------------------------
 * Copyright 2010-2013		Electronic Visualization Laboratory, University of Illinois at Chicago
 * Authors:										
 *  Alessandro Febretti		febret@gmail.com
 *-------------------------------------------------------------------------------------------------
 * Copyright (c) 2010-2013, Electronic Visualization Laboratory, University of Illinois at Chicago
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without modification, are permitted 
 * provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, this list of conditions 
 * and the following disclaimer. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the documentation and/or other 
 * materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE  GOODS OR SERVICES; LOSS OF 
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *************************************************************************************************/
#include <osgUtil/Optimizer>
#include <osgDB/ReadFile>
#include <osg/PositionAttitudeTransform>
#include <osg/MatrixTransform>
#include <osg/Light>
#include <osg/LightSource>
#include <osg/Material>

// PointIntersectior specific
#include <osg/Geometry>
#include <osg/Geode>
#include <osg/Point>
#include <osg/PolygonOffset>
#include <osgUtil/SmoothingVisitor>
#include <osgViewer/Viewer>
#include "PointIntersector"

#define OMEGA_NO_GL_HEADERS
#include <omega.h>
#include <omegaToolkit.h>
#include <omegaOsg/omegaOsg.h>

using namespace omega;
using namespace omegaToolkit;
using namespace omegaOsg;

String sModelName;
float sModelSize = 1.0f;

const osg::Vec4 normalColor(1.0f, 1.0f, 1.0f, 1.0f);
const osg::Vec4 selectedColor(1.0f, 0.0f, 0.0f, 1.0f);

///////////////////////////////////////////////////////////////////////////////
class SelectModelHandler : public osgGA::GUIEventHandler
{
public:
    SelectModelHandler( osg::Camera* camera )
    : _selector(0), _camera(camera) {}
    
    osg::Geode* createPointSelector()
    {
        osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array(1);
        (*colors)[0] = selectedColor;
        
        _selector = new osg::Geometry;
        _selector->setDataVariance( osg::Object::DYNAMIC );
        _selector->setUseDisplayList( false );
        _selector->setUseVertexBufferObjects( true );
        _selector->setVertexArray( new osg::Vec3Array(1) );
        _selector->setColorArray( colors.get() );
        _selector->setColorBinding( osg::Geometry::BIND_OVERALL );
        _selector->addPrimitiveSet( new osg::DrawArrays(GL_POINTS, 0, 1) );
        
        osg::ref_ptr<osg::Geode> geode = new osg::Geode;
        geode->addDrawable( _selector.get() );
        geode->getOrCreateStateSet()->setAttributeAndModes( new osg::Point(10.0f) );
        geode->getOrCreateStateSet()->setMode( GL_LIGHTING, osg::StateAttribute::OFF );
        return geode.release();
    }
    
    virtual bool handle( const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa )
    {
        if ( ea.getEventType()!=osgGA::GUIEventAdapter::RELEASE ||
             ea.getButton()!=osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON ||
             !(ea.getModKeyMask()&osgGA::GUIEventAdapter::MODKEY_CTRL) )
            return false;
        
        osgViewer::View* viewer = dynamic_cast<osgViewer::View*>(&aa);
        if ( viewer )
        {
            osg::ref_ptr<PointIntersector> intersector =
                new PointIntersector(osgUtil::Intersector::WINDOW, ea.getX(), ea.getY());
            osgUtil::IntersectionVisitor iv( intersector.get() );
            viewer->getCamera()->accept( iv );
            
            if ( intersector->containsIntersections() )
            {
                osg::Vec3Array* selVertices = dynamic_cast<osg::Vec3Array*>( _selector->getVertexArray() );
                if ( !selVertices ) return false;
                
                PointIntersector::Intersection result = *(intersector->getIntersections().begin());
                selVertices->front() = result.getWorldIntersectPoint();
                selVertices->dirty();
                _selector->dirtyBound();
            }
        }
        return false;
    }
    
	virtual bool omghandle(const Event& evt)
    {
		// getRayFromEvent()
		DisplaySystem* ds = SystemManager::instance()->getDisplaySystem();
		Ray r;
		bool res = ds->getViewRayFromEvent(evt, r);

		osg::Vec3 origin = osg::Vec3( r.getOrigin().x(), r.getOrigin().y(), r.getOrigin().z() );
		osg::Vec3 direction = osg::Vec3( r.getDirection().x(), r.getDirection().y(), r.getDirection().z() );

        osg::ref_ptr<PointIntersector> intersector =
			new PointIntersector( origin, direction * 1000 );
        osgUtil::IntersectionVisitor iv( intersector.get() );
        //viewer->getCamera()->accept( iv );
         
        if ( intersector->containsIntersections() )
        {
            osg::Vec3Array* selVertices = dynamic_cast<osg::Vec3Array*>( _selector->getVertexArray() );
            if ( !selVertices ) return false;
            
            PointIntersector::Intersection result = *(intersector->getIntersections().begin());
            selVertices->front() = result.getWorldIntersectPoint();
            selVertices->dirty();
            _selector->dirtyBound();
        }
        return false;
    }

protected:
    osg::ref_ptr<osg::Geometry> _selector;
    osg::observer_ptr<osg::Camera> _camera;
};

///////////////////////////////////////////////////////////////////////////////
class OsgViewer: public EngineModule
{
public:
	OsgViewer(): EngineModule("OsgViewer")
	{
		myOsg = new OsgModule();
		ModuleServices::addModule(myOsg); 
	}

	virtual void initialize();
	virtual void update(const UpdateContext& context);
	virtual void handleEvent(const Event& evt);

private:
	OsgModule* myOsg;
	SceneNode* mySceneNode;
	Actor* myInteractor;
	osg::Light* myLight;
	SelectModelHandler* selector;

	float myYaw;
	float myPitch;

};

///////////////////////////////////////////////////////////////////////////////
osg::Geometry* createSimpleGeometry()
{
    osg::ref_ptr<osg::Vec3Array> vertices = new osg::Vec3Array(8);
    (*vertices)[0].set(-0.5f,-0.55f,-0.15f);
    (*vertices)[1].set( 0.1f,-0.45f,-0.45f);
    (*vertices)[2].set( 0.2f, 0.35f,-0.35f);
    (*vertices)[3].set(-0.3f, 0.35f,-0.55f);
    (*vertices)[4].set(-0.45f,-0.51f, 0.4f);
    (*vertices)[5].set( 0.53f,-0.5f, 0.3f);
    (*vertices)[6].set( 0.51f, 0.35f, 0.2f);
    (*vertices)[7].set(-0.55f, 0.54f, 0.1f);
    
    osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array(1);
    (*colors)[0] = normalColor;
    
    osg::ref_ptr<osg::DrawElementsUInt> indices = new osg::DrawElementsUInt(GL_QUADS, 24);
    (*indices)[0] = 0; (*indices)[1] = 1; (*indices)[2] = 2; (*indices)[3] = 3;
    (*indices)[4] = 4; (*indices)[5] = 5; (*indices)[6] = 6; (*indices)[7] = 7;
    (*indices)[8] = 0; (*indices)[9] = 1; (*indices)[10]= 5; (*indices)[11]= 4;
    (*indices)[12]= 1; (*indices)[13]= 2; (*indices)[14]= 6; (*indices)[15]= 5;
    (*indices)[16]= 2; (*indices)[17]= 3; (*indices)[18]= 7; (*indices)[19]= 6;
    (*indices)[20]= 3; (*indices)[21]= 0; (*indices)[22]= 4; (*indices)[23]= 7;
    
    osg::ref_ptr<osg::Geometry> geom = new osg::Geometry;
    geom->setDataVariance( osg::Object::DYNAMIC );
    geom->setUseDisplayList( false );
    geom->setUseVertexBufferObjects( true );
    geom->setVertexArray( vertices.get() );
    geom->setColorArray( colors.get() );
    geom->setColorBinding( osg::Geometry::BIND_OVERALL );
    //geom->addPrimitiveSet( indices.get() );
    geom->addPrimitiveSet( new osg::DrawArrays(GL_POINTS, 0, 8) );

    osgUtil::SmoothingVisitor::smooth( *geom );
    return geom.release();
}

///////////////////////////////////////////////////////////////////////////////
void OsgViewer::initialize()
{

	// The node containing the scene
	osg::Node* node = NULL;

	// The root node (we attach lights and other global state properties here)
	// Set the root to be a lightsource to attach a light to it to illuminate the scene
	osg::Group* root = new osg::Group();

	// Load osg object
	if(SystemManager::settingExists("config/scene"))
	{
		Setting& sscene = SystemManager::settingLookup("config/scene");
		sModelName = Config::getStringValue("filename", sscene, sModelName);
		sModelSize = Config::getFloatValue("size", sscene, sModelSize);
	}

	osg::ref_ptr<osg::Geode> geode = new osg::Geode;
    geode->addDrawable( createSimpleGeometry() );
    //geode->getOrCreateStateSet()->setAttributeAndModes( new osg::PolygonOffset(1.0f, 1.0f) );
	geode->getOrCreateStateSet()->setAttributeAndModes( new osg::Point(10.0f) );
    geode->getOrCreateStateSet()->setMode( GL_LIGHTING, osg::StateAttribute::OFF );

	osg::ref_ptr<osg::MatrixTransform> trans = new osg::MatrixTransform;
    trans->addChild( geode.get() );
    trans->setMatrix( osg::Matrix::translate(0.0f, 0.0f, 1.0f) );

	osgViewer::Viewer viewer;
    selector = new SelectModelHandler( viewer.getCamera() );
    
	getEngine()->getDefaultCamera()->getViewTransform();

    root->addChild( trans.get() );
    root->addChild( selector->createPointSelector() );  // Caution: It has bound, too

	/*
	if(sModelName == "")
	{
		owarn("No model specified!!");
		return;
	}

	String path;
	if(DataManager::findFile(sModelName, path))
	{
		node = osgDB::readNodeFile(path);

		if (node == NULL) 
		{
			ofwarn("!Failed to load model: %1% (unsupported file format or corrupted data)", %path);
			return;
		}

		// Resize the model to make it sModelSize meters big.
		float r = node->getBound().radius() * 2;
		float scale = sModelSize / r;
		osg::PositionAttitudeTransform* pat = new osg::PositionAttitudeTransform();
		pat->setScale(osg::Vec3(scale, scale, scale));
		pat->addChild(node);
		node = pat;

		root->addChild(node);

		//Optimize scenegraph
		osgUtil::Optimizer optOSGFile;
		optOSGFile.optimize(node);
	}
	else
	{
		ofwarn("!File not found: %1%", %sModelName);
	}
	*/

	// Create an omegalib scene node and attach the osg node to it. This is used to interact with the 
	// osg object through omegalib interactors.
	//OsgSceneObject* oso = new OsgSceneObject(node);
	mySceneNode = new SceneNode(getEngine());
	//mySceneNode->addComponent(oso);
	mySceneNode->setBoundingBoxVisible(true);
	getEngine()->getScene()->addChild(mySceneNode);
	getEngine()->getDefaultCamera()->focusOn(getEngine()->getScene());

    // Set the interactor style used to manipulate meshes.
	if(SystemManager::settingExists("config/interactor"))
	{
		Setting& sinteractor = SystemManager::settingLookup("config/interactor");
		myInteractor = ToolkitUtils::createInteractor(sinteractor);
		if(myInteractor != NULL)
		{
			ModuleServices::addModule(myInteractor);
		}
	}

	if(myInteractor != NULL)
	{
		myInteractor->setSceneNode(mySceneNode);
	}

	// Set the osg node as the root node
	myOsg->setRootNode(root);

	// Setup shading
	myLight = new osg::Light;
    myLight->setLightNum(0);
    myLight->setPosition(osg::Vec4(0.0, 2, 1, 1.0));
    myLight->setAmbient(osg::Vec4(0.1f,0.1f,0.2f,1.0f));
    myLight->setDiffuse(osg::Vec4(1.0f,1.0f,1.0f,1.0f));
	myLight->setSpotExponent(0);
	myLight->setSpecular(osg::Vec4(0.0f,0.0f,0.0f,1.0f));

	osg::LightSource* ls = new osg::LightSource();
	ls->setLight(myLight);
	//ls->setLocalStateSetModes(osg::StateAttribute::ON);
	ls->setStateSetModes(*root->getOrCreateStateSet(), osg::StateAttribute::ON);

	root->addChild(ls);
}

///////////////////////////////////////////////////////////////////////////////
void OsgViewer::handleEvent(const Event& evt)
{
	if(evt.getServiceType() == Service::Pointer)
	{
		selector->omghandle(evt);
	}
}

///////////////////////////////////////////////////////////////////////////////
void OsgViewer::update(const UpdateContext& context) 
{
}

///////////////////////////////////////////////////////////////////////////////
// Application entry point
int main(int argc, char** argv)
{
	Application<OsgViewer> app("osgviewer");
	oargs().newNamedString(':', "model", "model", "The osg model to load", sModelName);
	oargs().newNamedString('s', "size", "size", "The screen size of the model in meters (default: 1)", sModelName);
    return omain(app, argc, argv);
}
